local ShmupCollision = {
	Category_Default = 1,
	Category_Camera = 2,
	Category_CameraEdge = 3,
	Category_PlayerTeam = 4,
	Category_PlayerShot = 5,
	Category_PlayerBomb = 6,
	Category_NPCTeam = 7,
	Category_NPCShot = 8,
	Category_NPCInCover = 9,
}

return ShmupCollision
