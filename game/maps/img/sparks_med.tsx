<?xml version="1.0" encoding="UTF-8"?>
<tileset name="sparks_med" tilewidth="64" tileheight="64" tilecount="16" columns="16">
 <tileoffset x="-32" y="32"/>
 <properties>
  <property name="row_explosion" value="0"/>
 </properties>
 <image source="sparks_med.png" width="1024" height="64"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="64"/>
   <frame tileid="1" duration="64"/>
   <frame tileid="2" duration="64"/>
   <frame tileid="3" duration="64"/>
   <frame tileid="4" duration="64"/>
   <frame tileid="5" duration="64"/>
   <frame tileid="6" duration="64"/>
   <frame tileid="7" duration="64"/>
   <frame tileid="8" duration="64"/>
   <frame tileid="9" duration="64"/>
  </animation>
 </tile>
</tileset>
