<?xml version="1.0" encoding="UTF-8"?>
<tileset name="boat" tilewidth="128" tileheight="64" tilecount="1" columns="1">
 <tileoffset x="-64" y="64"/>
 <image source="boat.png" width="128" height="64"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="6" x="-64" y="64" width="112" height="48"/>
   <object id="8" x="16" y="64" width="48" height="48">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
</tileset>
