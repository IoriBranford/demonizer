# Demonizer

2D scrolling shooter prototype demo - by Iori Branford

## About

The runaway princess returns from the "evil" demon realm as a succubus, capturing people to form her succubus squad and power it up with human spirit energy. To end the tyranny of her former kingdom she will corrupt it into a new demon realm.

## How to Play

Play with the keyboard, or a controller or mouse with three buttons. On keyboard, the arrow keys are the directions and the Z,X,C keys are Buttons 1,2,3.

On touchscreen, swipe with one finger for directions. Button 1 is on whenever a finger is onscreen. For Button 2, while holding one finger onscreen, press and hold a second finger. For Button 3, while holding two fingers onscreen, tap with a third finger.

- Push directions to move your succubus squad in formation.
	- Move into unarmed or defeated humans to capture them. Women become succubi and join your squad. Men give increasing point bonuses and energy for bombs; succubi holding enough men unlock special capturing abilities in focus mode.
- Press and hold Button 1 to rapid-fire demonic energy bolts at hostile humans and war machines in front of you.
	- When you defeat enemy humans, you have limited time to capture them before they bleed out or fall to their deaths. Any human's death also kills all of your captives, removing your special abilities and resetting the capture point bonus.
- Press and hold Button 2 to enter focus mode.
	- Your movement slows down for precision dodging. Your special capturing ability is increased capture radius.
	- Allies assume a more aggressive formation. When firing they autoaim at nearby enemies. Their special capturing ability is flying out to capturable humans.
- Press Button 3 to detonate a bomb in front of you, if you have enough bomb energy for one.
	- The bomb blast damages enemies, blocks their fire, and sends capturable humans to you.
	- Capture men to gain bomb energy, and hold onto them to slowly charge more bomb energy over time.
- Press Escape key to pause the game. Click on an option with the mouse or press Escape key again to resume.

## Links

Game site: https://ioribranford.itch.io/demonizer/

Game source: https://github.com/ioribranford/demonizer
